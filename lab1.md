# INF214 H23 - Lab 1 - Concurrency in Java - for week 36

## Presentation by a group leader

* __Linked List__ (_based on MittUiB pre-recorded video 7_)

## Tasks to solve before the group session and to discuss during the group session

### Task 1
Assume we have variables `x`, `y` and `z`, which all are initialized to 0.
Suppose we have two threads which are as follows.
What are the possible values of variable `x` after running the threads concurrently?
Explain your answer.


```java
Thread t1 = new Thread() {
    public void run() {
        x = y + z;
    }
};

Thread t2 = new Thread() {
    public void run() {
        y = 1;
        z = 2;
    }
};
```

---


### Task 2
Implement method `run_both`, which takes two functions as parameters, and calls each of them in a new thread.
The method must return a tuple with the result of the both functions.
You may use any JVM-language (for example, Java, Kotlin, Scala, ...) for this task. 

* Please note that Task 1 and Task 2 are **not related** to each other.
* _UPD 31.8.2023 2046CET_: An outline of code for Task 2 is available here: https://git.app.uib.no/Mikhail.Barash/inf214-h23/-/blob/main/lab1-task2-outline.java


## Reading documentation

* Make sure you know the difference between methods `.join`, `.sleep`, and `.yield` of the Java `Thread` class.


## Early preparation for the exam

* Make a small cheat sheet where you list various ways to create a thread in Java.
You many want to "browse literature" (i.e., to Google :blush:). 



